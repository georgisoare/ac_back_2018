-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Gazdă: 127.0.0.1
-- Timp de generare: nov. 05, 2018 la 10:49 PM
-- Versiune server: 10.1.36-MariaDB
-- Versiune PHP: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Bază de date: `ac`
--

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `register`
--

CREATE TABLE `register` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `birthdate` date NOT NULL,
  `department` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Eliminarea datelor din tabel `register`
--

INSERT INTO `register` (`id`, `firstname`, `lastname`, `phone`, `email`, `facebook`, `birthdate`, `department`) VALUES
(1, 'Andrei', 'Ion', '0729601114', 'ajgieg@gmail.com', 'wehewhweh', '2017-02-12', 'it'),
(2, 'Georgi', 'Costache', '1234567891', 'dfghgf', 'sdfdfd', '1999-04-04', 'it'),
(3, 'Georgi', 'Soare', '0743427963', 'georgisoare@yahoo.com', 'Georgiana Soare', '1999-08-03', 'it'),
(4, 'Nicolla', 'Soare', '0740817212', 'nicolla.soare@yahoo.com', 'Nicolla Soare', '1997-01-01', 'it'),
(5, 'Alina', 'Ciolpan', '0745362415', 'alina.ciolpan@yahoo.com', 'Alina Ang', '2000-02-02', 'it');

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `register2`
--

CREATE TABLE `register2` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `cnp` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `birth` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `question` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Eliminarea datelor din tabel `register2`
--

INSERT INTO `register2` (`id`, `firstname`, `lastname`, `phone`, `email`, `cnp`, `facebook`, `birth`, `department`, `question`) VALUES
(1, 'Alexandru', 'Bisag', '0729601114', 'bisagalexstefan@gmail.com', '1234567891234', 'facebook.com/bisagalexstefan', '1997-07-28', 'it', 'Because');

--
-- Indexuri pentru tabele eliminate
--

--
-- Indexuri pentru tabele `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`id`);

--
-- Indexuri pentru tabele `register2`
--
ALTER TABLE `register2`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pentru tabele eliminate
--

--
-- AUTO_INCREMENT pentru tabele `register`
--
ALTER TABLE `register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pentru tabele `register2`
--
ALTER TABLE `register2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
