<?php
require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$facebook = "";
$birth = "";
$department = "";
$error = 0;
$error_text = "";


if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}


if(empty($firstname) || empty($lastname) || empty($phone) || empty($email) || empty($facebook) || empty($birth)){
	$error = 1;
	$error_text = "One or more fields are empty!";
}

if(strlen($firstname) < 3 || strlen($lastname) < 3){
	$error = 1;
	$error_text = "First or Last name is shorter than expected!";
}

if(!is_numeric($phone) || strlen($phone)!=10){
	$error = 1;
	$error_text = "Phone number is not valid";
}

try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

    echo $response;
    return;

}

if($error == 0) {

    $stmt2 = $con -> prepare("INSERT INTO register(firstname,lastname,phone,email,facebook,birthdate,department) VALUES(:firstname,:lastname,:phone,:email,:facebook,:birth,:department)");

    $stmt2 -> bindParam(':firstname',$firstname);
    $stmt2 -> bindParam(':lastname',$lastname);
    $stmt2 -> bindParam(':phone',$phone);
    $stmt2 -> bindParam(':email',$email);
    $stmt2 -> bindParam(':facebook',$facebook);
    $stmt2 -> bindParam(':birth',$birth);
    $stmt2 -> bindParam(':department',$department);

    if(!$stmt2->execute()){

        $errors['connection'] = "Database Error";

    }
    else {
        echo "Success!";
    }
}
 else {echo $error_text;
    	return;}
  
  ?>
