<?php 

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$facebook = "";
$birth = "";
$department = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$facultate = "";
$sex = "";
$error=0;

if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}

if(isset($_POST['facultate'])){
	$facultate = $_POST['facultate'];
}

if(isset($_POST['sex'])){
	$sex = $_POST['sex'];
}

if(empty($firstname) || empty($lastname) || empty($phone) empty($email) || empty($cnp) || empty($facebook) || empty($birth) || empty($department) || empty($question) || empty($captcha_generated) || empty($captcha_inserted) || empty($check) || empty($facultate)) {
	$error = 1;
	$error_text = "One or more fields are empty";
}
if(strlen($firstname) < 3 || strlen($firstname) > 20)  {
	$error = 1;
	$error_text = "Firt name is shorter or longer than expected!";
}
if(strlen($lastname) < 3 || strlen($lastname) > 20)  {
	$error = 1;
	$error_text = "Last name is shorter or longer than expected!";
}
if(strlen($question) < 15)  {
	$error = 1;
	$error_text = "Question is not valid";
	}
if(!is_numeric($phone) || strlen($phone)!=10){
	$error = 1;
	$error_text = "Phone number is not valid";
}

for ($i=0;$i<strlen($email);$i++){
	$arond=0;
	$punct=0;
	if ($email[$i]=='@') $arond++;
	if ($email[$i]=='.') $punct++;
	if ($i==strlen($email) && ($arond!=1 || $punct!=1)){
		$error=1;
		$error_text= "Email gresit!";
	}
}
if(strlen($cnp)!=13 || ($cnp[0]!=1 && $cnp[0]!=2 && $cnp[0]!=3 && $cnp[0]!=4 && $cnp[0]!=5 && $cnp[0]!=6)){
	$error = 1;
	$error_text = "CNP is not valid";
}

if (strlen($facultate) < 3 || strlen($facultate) > 30){
	$error = 1;
	$error_text = "Facultate is not valid";
}

if ($birth[0]*1000+$birth[1]*100+$birth[2]*10+$birth[3]>2001 || $birth[0]*1000+$birth[1]*100+$birth[2]*10+$birth[3]<=1919){
	$error=1;
	$error_text= "Birth is not valid";
}

if (strlen($captcha_generated)==strlen($captcha_inserted)){
	for ($i=0;$i<=strlen($captcha_inserted);$i++){
		if ($captcha_generated[$i]!=$captcha_inserted[$i]){
			$error=1;
			$error_text="Captcha is not valid";
			break;
		}
	}
}

if ($cnp[1]!=$birth[2] || $cnp[2]!=$birth[3] || $cnp[3]!=$birth[5] || $cnp[4]!=$birth[6] || $cnp[5]!=$birth[8] || $cnp[6]!=$birth[9]){
	$error=1;
	$error_text= "Birth and CNP not valid";
}

$link="https://www.facebook.com/";
for($i=1;$i<strlen($link);$i++){
	if($link[$i]!=$facebook[$i]){
		$error=1;
		$error_text= "Facebook not valid";
	}
}

try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;

}

$query=$con -> prepare("SELECT 'email' FROM 'register2' WHERE 'email'=?");
$query -> bindValue(1,$email);
$query -> execute();
if($query -> rowCount()!=0){
	$error=1;
	$error_text= "Email is not valid";
}
$query2=$con -> prepare("SELECT * FROM 'register2'");
$query2 -> execute();
if($query2->rowCount()>=50){
	$error=1;
	$error_text="Too many";
}

if ($error==0){
$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,phone,email,cnp,facebook,birth,department,question,facultate,sex) VALUES(:firstname,:lastname,:phone,:email,:cnp,:facebook,:birth,:department,:question,:facultate,:sex)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':cnp',$cnp);
$stmt2 -> bindParam(':facebook',$facebook);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':department',$department);
$stmt2 -> bindParam(':question',$question);
$stmt2 -> bindParam(':facultate',$facultate);

if ($cnp[0]==1 || $cnp[0]==3 || $cnp[0]==5){
	$sex = 'M';
	$stmt2 -> bindParam(':sex', $sex);
}
	else {
		$sex='F';
		$stmt2 -> bindParam(':sex', $sex);
	}
if(!$stmt2->execute()){

    $errors['connection'] = "Database Error";

}else{
    echo "Succes";
}
}
	else {
		echo $error_text;
	}
?>

